#!/bin/bash

# Copyright (c) 2023, ARM Limited and Contributors.
# All rights reserved.
# SPDX-Licence-Identifier: BSD-3-Clause

set -eu

SCRIPT_DIR=`dirname "${BASH_SOURCE[0]}"`
cd "$SCRIPT_DIR/.."

grep jetstream examples/data/js-one-column.csv | ./plot --width=0.3 --no-legend-title --legend-position=top --out=examples/js-narrow.svg
